const Strategy = require("passport-local").Strategy;
const User = require("../../model/userModel");

const SigninStrategy = new Strategy(
  {
    passReqToCallback: true,
    usernameField: "email",
    passwordField: "googleId",
    nameField: "name",
  },
  function (req, email, password, done) {
    console.log(req.body);
    User.findOne({ email: req.body.email })
      .lean()
      .exec((err, user) => {
        if (!user) {
          console.log("entered in user module");
          let newUser = new User({
            email: req.body.email,
            name: req.body.name,
            password: req.body.googleId,
            confirmPassword: req.body.googleId,
          });
          console.log("newUser", newUser);
          newUser.save((error, inserted) => {
            if (error) {
              console.log();
            }

            return done(null, inserted);
          });
        }
        if (user) {
          return done(null, "User Login success");
        }
      });
  }
);

module.exports = SigninStrategy;
