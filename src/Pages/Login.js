import React, { Fragment, useEffect, useState } from "react";
import {
  Button,
  TextField,
  Typography,
  InputAdornment,
  IconButton,
  CircularProgress,
  Avatar,
  Box,
  Container,
  Grid,
} from "@material-ui/core";
import { LockOutlined } from "@material-ui/icons";
import { Link } from "react-router-dom";
import { useForm } from "react-hook-form";
import { Visibility, VisibilityOff } from "@material-ui/icons";
import { useHistory } from "react-router-dom";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { loginUser } from "../redux/actions/authAction";
import { useDispatch, useSelector } from "react-redux";
import { makeStyles } from "@material-ui/styles";
import { CLEAR_ERRORS, LOGIN_SUCCESS } from "../redux/types";
import { GoogleLogin } from "react-google-login";
import axiosInstance from "../utils/axiosInstance";
const useStyles = makeStyles({
  container: {
    marginTop: "20%",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
});

const LoginForm = (props) => {
  const classes = useStyles();
  const [showPass, setShowPass] = useState(false);
  const schema = yup.object().shape({
    email: yup
      .string()
      .required("Email is Required")
      .email("Please Enter a Valid Email"),
  });
  const handleClickShowPassword = () => {
    setShowPass(!showPass);
  };
  useEffect(() => {
    dispatch({ type: CLEAR_ERRORS });
  }, []);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const state = useSelector((state) => state.UI);
  const [message, setMessage] = useState(null);
  const [status, setStatus] = useState(null);
  const dispatch = useDispatch();
  const history = useHistory();
  const onSubmit = (userData) => {
    setMessage(null);
    setStatus(null);
    dispatch(loginUser(userData, history));
  };

  if (state.isAuthenticated) {
    history.push("/");
  }

  const responseSuccessGoogle = async ({ profileObj }) => {
    console.log(profileObj);
    axiosInstance({
      method: "post",
      url: "/auth/googlesignin",
      data: {
        googleId: profileObj.googleId,
        email: profileObj.email,
      },
    })
      .then((res) => console.log(res.data))
      .catch((err) => console.log(err));
  };
  const responseErrorGoogle = (response) => {
    console.log(response);
  };
  let form = (
    <Box
      component="form"
      noValidate
      onSubmit={handleSubmit(onSubmit)}
      sx={{ mt: 3 }}
    >
      <Grid container spacing={2}>
        <Grid item xs={12} sm={12}>
          <TextField
            error={errors.email ? true : false}
            helperText={errors.email && errors.email?.message}
            label="Email address"
            {...register("email")}
            fullWidth
            autoFocus
            type="email"
            variant="outlined"
          />
        </Grid>
        <Grid item xs={12} sm={12}>
          <TextField
            error={errors.password ? true : false}
            helperText={errors.password && errors.password?.message}
            label="Password"
            {...register("password")}
            type={showPass ? "text" : "password"}
            variant="outlined"
            fullWidth
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                  >
                    {showPass ? <Visibility /> : <VisibilityOff />}
                  </IconButton>
                </InputAdornment>
              ),
            }}
          />
        </Grid>
      </Grid>
      <Typography
        color={status ? (status === "success" ? "primary" : "error") : "error"}
      >
        {message ? message : null}
        {state.errors ? state.errors : null}
      </Typography>
      <Box sx={{ mt: 4, mb: 2 }}>
        <Grid>
          <Button type="submit" fullWidth color="primary" variant="contained">
            {state.loading ? (
              <CircularProgress size="1rem" color="secondary" />
            ) : (
              "Sign In"
            )}
          </Button>
        </Grid>
      </Box>
      <Box sx={{ mt: 4, mb: 2 }}>
        <Grid>
          <GoogleLogin
            fullWidth
            clientId="58019614744-in61pg7gcecn8qnqf548dsopjpo3qig5.apps.googleusercontent.com"
            buttonText="Login with Google"
            onSuccess={responseSuccessGoogle}
            onFailure={responseErrorGoogle}
            cookiePolicy={"single_host_origin"}
          />
        </Grid>
      </Box>
      <Box sx={{ mt: 3, mb: 2 }}>
        <Grid container justifyContent="flex-end">
          <Grid item>
            <Link to="/signup">
              <Typography>Don't have an account? Sign Up</Typography>
            </Link>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );

  return (
    <Fragment>
      <Container component="main" maxWidth="xs">
        {/* <CssBaseline /> */}
        <Box className={classes.container}>
          <Avatar sx={{ m: 1, backgroundColor: "green" }}>
            <LockOutlined />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          {form}
        </Box>
      </Container>
    </Fragment>
  );
};

export default LoginForm;
