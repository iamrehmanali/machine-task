//UI reducers
export const SET_ERRORS = "SET_ERRORS";
export const LOADING_UI = "LOADING_UI";
export const CLEAR_ERRORS = "CLEAR_ERRORS";
export const SERVER_ERROR = "SERVER_ERROR";
export const SIGNUP_SUCCESS = "SIGNUP_SUCCESS";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const SET_ERROR = "SET_ERROR";
export const USER_LOGOUT = "USER_LOGOUT";
