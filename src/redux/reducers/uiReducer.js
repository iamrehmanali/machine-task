import {
  SET_ERRORS,
  CLEAR_ERRORS,
  LOADING_UI,
  SERVER_ERROR,
  SIGNUP_SUCCESS,
  SET_ERROR,
  LOGIN_SUCCESS,
  USER_LOGOUT,
} from "../types";

const userInfoFromStorage = localStorage.getItem("user_id")
  ? JSON.parse(localStorage.getItem("user_id"))
  : null;

const initialState = {
  loading: false,
  serverError: false,
  errors: null,
  signUpSuccess: false,
  isAuthenticated: userInfoFromStorage ? true : false,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SET_ERRORS:
      return {
        ...state,
        loading: false,
        errors: action.payload.errors,
        serverError: false,
      };
    case SET_ERROR:
      return {
        ...state,
        loading: false,
        errors: action.payload.message,
        serverError: false,
      };
    case SERVER_ERROR:
      return {
        ...state,
        loading: false,
        serverError: true,
        errors: null,
      };
    case CLEAR_ERRORS:
      return {
        ...state,
        loading: false,
        errors: null,
      };
    case LOADING_UI:
      return {
        ...state,
        loading: true,
        serverError: false,
        signUpSuccess: false,
      };
    case SIGNUP_SUCCESS:
      return {
        ...state,
        signUpSuccess: true,
        isAuthenticated: true,
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        isAuthenticated: true,
      };
    case USER_LOGOUT:
      return {
        loading: false,
        serverError: false,
        errors: null,
        signUpSuccess: false,
        isAuthenticated: false,
      };
    default:
      return state;
  }
}
