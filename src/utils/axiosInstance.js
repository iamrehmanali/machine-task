import axios from "axios";

const axiosInstance = axios.create({
  baseURL: "http://localhost:5002/api",
});

axiosInstance.interceptors.request.use((req) => {
  req.headers.authentication = "Bearer token";
  req.headers.ContentType = "application/json";
  return req;
});

export default axiosInstance;
