const express = require("express");
const router = express.Router();
const authController = require("../controller/authController");

router.post("/signup", authController.signup);
router.post("/login", authController.login);

router.use("/googlesignin", authController.googlesignin);
router.use("/googlesignup", authController.googlesignup);

module.exports = router;
