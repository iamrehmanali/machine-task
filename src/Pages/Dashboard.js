import React from "react";
import { Box, Grid, Typography } from "@material-ui/core";

import { CanvasJSChart } from "canvasjs-react-charts";
function Dashboard() {
  const options = {
    animationEnabled: true,
    exportEnabled: false,
    theme: "dark2",
    title: {
      text: "Simple Column Chart ",
    },
    axisY: {
      includeZero: true,
    },
    data: [
      {
        type: "column",
        indexLabelFontColor: "green",
        indexLabelPlacement: "outside",
        dataPoints: [
          { x: 10, y: 71 },
          { x: 20, y: 55 },
          { x: 30, y: 50 },
          { x: 40, y: 65 },
          { x: 50, y: 71 },
          { x: 60, y: 68 },
          { x: 70, y: 38 },
          { x: 80, y: 92, indexLabel: "Highest" },
          { x: 90, y: 54 },
          { x: 100, y: 60 },
          { x: 110, y: 21, indexLabel: "Lowest" },
          { x: 120, y: 49 },
          { x: 130, y: 36 },
        ],
      },
    ],
  };
  const options2 = {
    animationEnabled: true,
    title: {
      text: "Customer Satisfaction",
    },
    subtitles: [
      {
        text: "60% Positive",
        verticalAlign: "center",
        fontSize: 20,
        dockInsidePlotArea: true,
      },
    ],
    data: [
      {
        type: "doughnut",
        showInLegend: true,
        indexLabel: "{name}: {y}",
        yValueFormatString: "#,###'%'",
        dataPoints: [
          { name: "Unsatisfied", y: 7 },
          { name: "Very Unsatisfied", y: 10 },
          { name: "Very Satisfied", y: 60 },
          { name: "Satisfied", y: 9 },
          { name: "Neutral", y: 17 },
        ],
      },
    ],
  };
  const options3 = {
    animationEnabled: true,
    title: {
      text: "Nuclear Electricity Generation",
    },
    axisY: {
      title: "Net Generation (in Billion kWh)",
      suffix: " kWh",
    },
    data: [
      {
        type: "splineArea",
        xValueFormatString: "YYYY",
        yValueFormatString: "#,##0.## bn kW⋅h",
        showInLegend: true,
        legendText: "kWh = one kilowatt hour",
        dataPoints: [
          { x: new Date(2008, 0), y: 70.735 },
          { x: new Date(2009, 0), y: 74.102 },
          { x: new Date(2010, 0), y: 72.569 },
          { x: new Date(2011, 0), y: 72.743 },
          { x: new Date(2012, 0), y: 72.381 },
          { x: new Date(2013, 0), y: 71.406 },
          { x: new Date(2014, 0), y: 73.163 },
          { x: new Date(2015, 0), y: 74.27 },
          { x: new Date(2016, 0), y: 72.525 },
          { x: new Date(2017, 0), y: 73.121 },
        ],
      },
    ],
  };
  return (
    <Box mt={5}>
      <Grid container justifyContent="center" spacing={6}>
        <Grid item md={5}>
          <CanvasJSChart options={options} />
        </Grid>
        <Grid item md={5}>
          <CanvasJSChart options={options2} />
        </Grid>
        <Grid item md={10}>
          <CanvasJSChart options={options3} />
        </Grid>
      </Grid>
    </Box>
  );
}

export default Dashboard;
