import React, { Fragment } from "react";
import { AppBar, Button, Toolbar, Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/styles";
import { Link } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import { Logout } from "../redux/actions/authAction";

const useStyles = makeStyles(() => ({
  title: { flex: 1, color: "primary", textDecoration: "none" },
  buttonStyles: {
    color: "black",
    margin: "0 10px 0",
    display: "inline-block",
  },
  buttons: {
    marginRight: 60,
  },
  name: {
    fontStyle: "bold",
    fontSize: 32,
  },
}));
export default function ButtonAppBar() {
  const state = useSelector((state) => state.UI);

  const classes = useStyles();
  const history = useHistory();
  const dispatch = useDispatch();

  const logoutHandler = () => {
    dispatch(Logout());
    history.push("/Login");
  };
  const userLinks = (
    <Fragment>
      <Button
        className={classes.buttonStyles}
        variant="outlined"
        onClick={logoutHandler}
      >
        Logout
      </Button>
    </Fragment>
  );
  const loginLinks = (
    <Fragment>
      <Link to="/login">
        <Button className={classes.buttonStyles} variant="outlined">
          Login
        </Button>
      </Link>
      <Link to="/signup">
        <Button className={classes.buttonStyles} variant="outlined">
          Sign Up
        </Button>
      </Link>
    </Fragment>
  );
  return (
    <AppBar position="static" color="inherit" mb={4}>
      <Toolbar>
        <Link to="/" className={classes.title}>
          <Typography variant="h4" noWrap>
            Machine Test
          </Typography>
        </Link>
        <div className={classes.buttons}>
          {state.isAuthenticated ? userLinks : loginLinks}
        </div>
      </Toolbar>
    </AppBar>
  );
}
