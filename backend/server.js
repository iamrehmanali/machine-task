const config = require("dotenv");
const app = require("./app");
const connectDB = require("./config/DB");

config.config();
connectDB();

const PORT = 5002;
app.listen(PORT, console.log(`run server on ${PORT}`));
