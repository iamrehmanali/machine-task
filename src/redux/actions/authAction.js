import axiosInstance from "../../utils/axiosInstance";
import {
  CLEAR_ERRORS,
  LOADING_UI,
  LOGIN_SUCCESS,
  SERVER_ERROR,
  SET_ERROR,
  SIGNUP_SUCCESS,
  USER_LOGOUT,
} from "../types";

export const signupUser = (newUserdata, history) => (dispatch) => {
  dispatch({ type: LOADING_UI });
  axiosInstance
    .post("/auth/signup", newUserdata)
    .then((res) => {
      dispatch({
        type: SIGNUP_SUCCESS,
      });

      localStorage.setItem("user_id", JSON.stringify(res.data.data.user._id));
      localStorage.setItem("token", JSON.stringify(res.data.token));
      dispatch({ type: CLEAR_ERRORS });
      history.push("/");
    })
    .catch((err) => {
      // console.log(err.response.data);
      if (err.response) {
        dispatch({
          type: SET_ERROR,
          payload: err.response.data,
        });
      } else {
        dispatch({
          type: SERVER_ERROR,
        });
      }
    });
};

export const loginUser = (userData, history) => (dispatch) => {
  dispatch({ type: LOADING_UI });
  axiosInstance
    .post("/auth/login", userData)
    .then((res) => {
      dispatch({ type: LOGIN_SUCCESS });
      dispatch({ type: CLEAR_ERRORS });
      localStorage.setItem("user_id", JSON.stringify(res.data.data.user._id));
      localStorage.setItem("token", JSON.stringify(res.data.token));
      history.push("/");
    })
    .catch((err) => {
      if (err.response) {
        dispatch({
          type: SET_ERROR,
          payload: err.response.data,
        });
      } else {
        dispatch({
          type: SERVER_ERROR,
        });
      }
    });
};

export const Logout = () => (dispatch) => {
  localStorage.removeItem("user_id");
  localStorage.removeItem("token");
  dispatch({ type: USER_LOGOUT });
};
