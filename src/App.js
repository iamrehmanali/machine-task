import { BrowserRouter, Switch, Route } from "react-router-dom";
import Registration from "./Pages/Registration";
import login from "./Pages/Login";
import Home from "./Pages/Dashboard";
import ButtonAppBar from "./component/appBar";

import "./App.css";

function App() {
  return (
    <BrowserRouter>
      <ButtonAppBar />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/signup" component={Registration} />
        <Route exact path="/login" component={login} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
