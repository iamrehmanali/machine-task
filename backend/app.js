const express = require("express");
const app = express();
const authRouter = require("./router/authRouter");
const cors = require("cors");
const passport = require("./auth");

app.use(passport.initialize());
// app.use(passport.session());

// app.use(express.urlencoded({ extended: false }));
app.use(cors());
app.use(express.json());

app.use("/api/auth", authRouter);

module.exports = app;
