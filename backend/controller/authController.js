const User = require("../model/userModel");
const catchAsync = require("../utils/catchAsync");
const AppError = require("../utils/appError");
const create = require("../utils/generateToken");
const passport = require("../auth");
exports.signup = catchAsync(async (req, res, next) => {
  const newUser = await User.create(req.body);
  create.generateToken(newUser, 201, res);
});

exports.login = catchAsync(async (req, res, next) => {
  const { email, password } = req.body;

  if (!email) {
    return next(new AppError("Plaease provide email or password", 404));
  }
  const user = await User.findOne({ email }).select("+password");
  if (!user) {
    return next(new AppError("invalid email or password", 401));
  }
  await user.save({ validateBeforeSave: false });
  if (!user || !(await user.correctPassword(password, user.password))) {
    return next(new AppError("invalid email or password", 401));
  }
  create.generateToken(user, 200, res);
});

exports.googlesignin = catchAsync(async (req, res, next) => {
  passport.authenticate("google-signin", function (error, user, info) {
    if (error) {
      return res.status(500).json({
        message: error || "Something happend",
        error: error.message || "Server error",
      });
    }

    req.logIn(user, function (error, data) {
      if (error) {
        return res.status(500).json({
          message: error || "Something happend",
          error: error.message || "Server error",
        });
      }
    });

    user.isAuthenticated = true;
    return res.json(user);
  })(req, res);
});
exports.googlesignup = catchAsync(async (req, res, next) => {
  passport.authenticate("google-signup", function (error, user, info) {
    if (error) {
      return res.status(500).json({
        message: error || "Something happend",
        error: error.message || "Server error",
      });
    }
    req.logIn(user, function (error, data) {
      if (error) {
        return res.status(500).json({
          message: error || "Something happend",
          error: error.message || "Server error",
        });
      }
      return res.json(user);
    });
  })(req, res);
});
