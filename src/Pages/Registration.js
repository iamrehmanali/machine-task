import React, { Fragment, useEffect, useState } from "react";
import {
  Button,
  TextField,
  Typography,
  InputAdornment,
  IconButton,
  CircularProgress,
  Avatar,
  Box,
  Container,
  Grid,
} from "@material-ui/core";
import { LockOutlined } from "@material-ui/icons";
import { Link } from "react-router-dom";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { Visibility, VisibilityOff } from "@material-ui/icons";
import { useHistory } from "react-router-dom";
import { signupUser } from "../redux/actions/authAction";
import { useDispatch, useSelector } from "react-redux";
import { CLEAR_ERRORS } from "../redux/types";
import { GoogleLogin } from "react-google-login";
import axiosInstance from "../utils/axiosInstance";

const RegisterForm = (props) => {
  const [showPass, setShowPass] = useState(false);

  const schema = yup.object().shape({
    name: yup
      .string()
      .required("Name is Required")
      .min(3, "Minimum 3 Characters"),
    email: yup
      .string()
      .required("Email is Required")
      .email("Please Enter a Valid Email"),
    password: yup
      .string()
      .required("Password is required")
      .matches(
        /^((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*()"'.,<>;:`~|?-_]))(?=.{8,})/,
        "Must Contain 8 Characters, One Alphabet, One Number and one special case Character"
      ),
    confirmPassword: yup
      .string()
      .required("Please Confirm Your Password")
      .oneOf([yup.ref("password"), null], "Password Does not match"),
  });

  const handleClickShowPassword = () => {
    setShowPass(!showPass);
  };
  useEffect(() => {
    dispatch({ type: CLEAR_ERRORS });
  }, []);

  const {
    register,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(schema),
  });

  const state = useSelector((state) => state.UI);

  const [fromData, updateFormData] = useState({
    name: "",
    email: "",
    password: "",
    confirmPassword: "",
    gender: "",
    role: "",
    policy: "",
  });

  const dispatch = useDispatch();
  const history = useHistory();

  const onSubmit = (newUserdata) => {
    updateFormData(watch());
    dispatch({ type: CLEAR_ERRORS });
    dispatch(signupUser(newUserdata, history));
    if (state.errors) {
      updateFormData(watch());
    }
  };

  if (state.isAuthenticated) {
    history.push("/");
  }
  const responseSuccessGoogle = async ({ profileObj }) => {
    console.log(profileObj);
    axiosInstance({
      method: "post",
      url: "/auth/googlesignup",
      data: {
        googleId: profileObj.googleId,
        email: profileObj.email,
        name: profileObj.name,
      },
    })
      .then((res) => console.log(res.data))
      .catch((err) => console.log(err));
  };
  const responseErrorGoogle = (response) => {
    console.log(response);
  };
  let form = (
    <Box
      component="form"
      noValidate
      onSubmit={handleSubmit(onSubmit)}
      sx={{ mt: 3 }}
    >
      <Grid container spacing={2}>
        <Grid item xs={12} sm={12}>
          <TextField
            error={errors.name ? true : false}
            helperText={errors.name && errors.name?.message}
            label="Name"
            type="text"
            fullWidth
            {...register("name")}
            autoFocus
            variant="outlined"
          />
        </Grid>

        <Grid item xs={12}>
          <TextField
            error={errors.email ? true : false}
            helperText={errors.email && errors.email?.message}
            label="Email address"
            fullWidth
            {...register("email")}
            type="email"
            variant="outlined"
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            error={errors.password ? true : false}
            helperText={errors.password && errors.password?.message}
            label="Password"
            fullWidth
            {...register("password")}
            type={showPass ? "text" : "password"}
            variant="outlined"
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                  >
                    {showPass ? <Visibility /> : <VisibilityOff />}
                  </IconButton>
                </InputAdornment>
              ),
            }}
          />
        </Grid>
        <Grid item xs={12}>
          <TextField
            error={errors.confirmPassword ? true : false}
            helperText={
              errors.confirmPassword && errors.confirmPassword?.message
            }
            fullWidth
            label="Confirm Password"
            {...register("confirmPassword")}
            type={showPass ? "text" : "password"}
            variant="outlined"
          />
        </Grid>
      </Grid>
      <Typography color="error">
        {state.errors ? state.errors : null}
      </Typography>
      <Box sx={{ mt: 4, mb: 2 }}>
        <Grid>
          <Button type="submit" fullWidth variant="contained" color="primary">
            {state.loading ? (
              <CircularProgress size="1rem" color="secondary" />
            ) : (
              "Sign up"
            )}
          </Button>
        </Grid>
      </Box>
      <Box sx={{ mt: 3, mb: 2 }}>
        <Grid container justifyContent="flex-end">
          <Grid item>
            <Link to="/login">Already have an account? Sign in</Link>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );

  return (
    <Fragment>
      <Container component="main" maxWidth="xs">
        {/* <CssBaseline /> */}
        <Box
          sx={{
            marginTop: "10%",
            display: "flex",
            flexDirection: "column",
            alignItems: "center",
          }}
        >
          <Avatar style={{ m: 1, backgroundColor: "green" }}>
            <LockOutlined />
          </Avatar>
          <Typography component="h1" variant="h4">
            Sign up
          </Typography>
          {form}
        </Box>
        <Box>
          <GoogleLogin
            fullWidth
            clientId="58019614744-in61pg7gcecn8qnqf548dsopjpo3qig5.apps.googleusercontent.com"
            buttonText="SignUp with Google"
            onSuccess={responseSuccessGoogle}
            onFailure={responseErrorGoogle}
            cookiePolicy={"single_host_origin"}
          />
        </Box>
      </Container>
    </Fragment>
  );
};

export default RegisterForm;
